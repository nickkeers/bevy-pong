use bevy::prelude::*;
use bevy::text::BreakLineOn;
use crate::app_state::AppState;

pub struct MenuPlugin;

const TEXT_COLOR: Color = Color::rgb(0.9, 0.9, 0.9);

#[derive(Component)]
struct MenuScreen;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_systems(OnEnter(AppState::MainMenu), setup_menu)
            .add_systems(Update, menu.run_if(in_state(AppState::MainMenu)))
            .add_systems(OnExit(AppState::MainMenu), cleanup_menu);
    }
}

fn menu(
    input: Res<Input<KeyCode>>,
    mut game_state: ResMut<NextState<AppState>>,
) {
    if input.just_pressed(KeyCode::Space) {
        game_state.set(AppState::InGame);
    }
}

fn cleanup_menu(
    mut commands: Commands,
    query: Query<Entity, With<MenuScreen>>,
) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

pub fn setup_menu(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn((
        NodeBundle {
            style: Style {
                width: Val::Percent(100.0),
                height: Val::Percent(100.0),
                // center children
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                ..default()
            },
            ..default()
        }, MenuScreen
    ))
        .with_children(|parent| {
            parent
                .spawn(NodeBundle {
                    style: Style {
                        // This will display its children in a column, from top to bottom
                        flex_direction: FlexDirection::Column,
                        // `align_items` will align children on the cross axis. Here the main axis is
                        // vertical (column), so the cross axis is horizontal. This will center the
                        // children
                        align_items: AlignItems::Center,
                        ..default()
                    },
                    background_color: Color::BLACK.into(),
                    ..default()
                })
                .with_children(|parent| {
                    // Display two lines of text, the second one with the current settings
                    parent.spawn(
                        TextBundle::from_sections([
                            TextSection::new(
                                "We're in the Menu",
                                TextStyle {
                                    font_size: 80.0,
                                    color: TEXT_COLOR,
                                    ..default()
                                }),
                            TextSection::new("\n", TextStyle {
                                font_size: 80.0,
                                color: TEXT_COLOR,
                                ..default()
                            }),
                        ])
                            .with_style(Style {
                                margin: UiRect::all(Val::Px(50.0)),
                                ..default()
                            }),
                    );

                    parent.spawn(
                        TextBundle::from_section("Press space to enter game",
                                                 TextStyle {
                                                     font_size: 40.0,
                                                     color: TEXT_COLOR,
                                                     ..default()
                                                 },
                        ).with_style(Style {
                            align_items: AlignItems::Center,
                            margin: UiRect::bottom(Val::Px(50.0)),
                            ..default()
                        })
                    );
                });
        });
}

