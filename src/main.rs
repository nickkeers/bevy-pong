mod game;
mod menu;
mod app_state;

use bevy::app::Startup;
use bevy::DefaultPlugins;
use bevy::prelude::{App, Camera2dBundle, Commands, States};
use crate::app_state::AppState;


fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_state::<AppState>()
        .add_systems(Startup, setup)
        .add_plugins((menu::MenuPlugin, game::GamePlugin))
        .run();
}

fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}